package logica_modelo;

import persistencia_controlador.ConexionDB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Producto {

    // Atributos
    private int id;
    private String nombre;
    private int cantidad;
    private String categoria;
    private double precio;

    // Constructor
    public Producto(){}

    public Producto(String nombre, int cantidad, String categoria, double precio){
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.categoria = categoria;
        this.precio = precio;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getNombre(){
        return this.nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int getCantidad(){
        return this.cantidad;
    }

    public void setCantidad(int cantidad){
        this.cantidad = cantidad;
    }

    public String getCategoria(){
        return this.categoria;
    }

    public void setCategoria(String categoria){
        this.categoria = categoria;
    }

    public double getPrecio(){
        return this.precio;
    }

    public void setPrecio(double precio){
        this.precio = precio;
    }

    //no elimine ni modifique este metodo
    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", nombre=" + nombre +
                ", cantidad=" + cantidad + ", categoria=" + categoria +
                ", precio=" + precio + '}';
    }

    // retorna la lista de productos de la base de datos => SELECT
    public List<Producto> listarProductos(){
        // para utilizar una lista de datos de productos
        List<Producto> listaProductos = new ArrayList<>();
        // objeto de la base de datos, establecer conexión
        ConexionDB conexionBD = new ConexionDB();
        String sql = "SELECT * FROM productos;";
        try {
            // obtener el resultado
            ResultSet rs = conexionBD.cunsultarBD(sql);
            // declaramos objeto de la clase producto
            Producto prod;
            while (rs.next()){
                prod = new Producto();
                prod.setId(rs.getInt("id"));
                prod.setNombre(rs.getString("nombre"));
                prod.setCantidad(rs.getInt("cantidad"));
                prod.setCategoria(rs.getString("categoria"));
                prod.setPrecio(rs.getDouble("precio"));
                listaProductos.add(prod);
            }
        } catch (SQLException e){
            System.out.println("Error lista de prod... " + e.getMessage());
        } finally {
            conexionBD.cerrarConexion();
        }
        return listaProductos;
    }

    // ------------------------------------------------------------------------------
    // Agregada-----------
    // Retornaría en la lista un único producto respecto al id
    // retorna la lista de productos de la base de datos => SELECT
    public List<Producto> listarProductos_id(){
        // para utilizar una lista de datos de productos
        List<Producto> listaProductos = new ArrayList<>();
        // objeto de la base de datos, establecer conexión
        ConexionDB conexionBD = new ConexionDB();
        String sql = "SELECT * FROM productos WHERE id = " + id + ";";
        try {
            // obtener el resultado
            ResultSet rs = conexionBD.cunsultarBD(sql);
            // declaramos objeto de la clase producto
            Producto prod;
            while (rs.next()){
                prod = new Producto();
                prod.setId(rs.getInt("id"));
                prod.setNombre(rs.getString("nombre"));
                prod.setCantidad(rs.getInt("cantidad"));
                prod.setCategoria(rs.getString("categoria"));
                prod.setPrecio(rs.getDouble("precio"));
                listaProductos.add(prod);
            }
        } catch (SQLException e){
            System.out.println("Error lista de prod... " + e.getMessage());
        } finally {
            conexionBD.cerrarConexion();
        }
        return listaProductos;
    }
    // ------------------------------------------------------------------------------


    // guardar o insertar productos en la bd => INSERT
    public boolean guardarProducto(){
        ConexionDB conexionBD = new ConexionDB();
        /*String sql = "INSERT INTO productos(nombre, cantidad, categoria, precio) " +
                "VALUES(\"" + this.nombre + "\"," + this.cantidad + ",\"" +
                this.categoria + "\"," + this.precio + ");";*/
        String sql = "INSERT INTO productos(nombre,cantidad,categoria,precio)" +
                "VALUES('" + nombre + "'," + cantidad + ",'" +
                categoria + "'," + precio + ");";
        if (conexionBD.setAutoCommitBD(false)){ // para q la bd no confirme automáticamente el cambio...
            if (conexionBD.insertarBD(sql)){
                conexionBD.commitBD(); // confirma el cambio a la bd
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }

    // actualizar o realizar cambios a productos de la base de datos => UPDATE
    public boolean actualizarProducto(){
        ConexionDB conexionBD = new ConexionDB();
        String sql = "UPDATE productos SET nombre='" + nombre +
                "',cantidad=" + cantidad + ",categoria='" + categoria +
                "',precio=" + precio + " WHERE id=" + id + ";";
        if (conexionBD.setAutoCommitBD(false)){// para q la bd no confirme automáticamente el cambio...
            if (conexionBD.actualizarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }

    // eliminar o remover productos de la base de datos => DELETE
    public boolean eliminarProducto(){
        ConexionDB conexionBD = new ConexionDB();
        String sql = "DELETE FROM productos WHERE id=" + id + ";";
        if (conexionBD.setAutoCommitBD(false)){ // para q la bd no confirme automáticamente el cambio...
            if (conexionBD.actualizarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }

}
