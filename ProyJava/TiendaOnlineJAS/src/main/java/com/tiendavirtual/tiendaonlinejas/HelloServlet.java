package com.tiendavirtual.tiendaonlinejas;

import java.io.*;

import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        for (int i = 0; i < 6; i++) {
            out.println("<h2>" + "Variable i = " + i + "</h2>");
        }
        out.println("</body></html>");
    }

    public void destroy() {
    }
}