package presentacion_vista;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import logica_modelo.Producto;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "MostrarProductos", value = "/MostrarProductos")
public class MostrarProductos extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        out.println("<html><body>");
        out.println("<h1>" + "Página para Mostrar Contenido de la Base de Datos" + "</h1>");

        Producto prod = new Producto();
        List<Producto> listaProd = prod.listarProductos();

        out.println("<table border=\"3\">");
            out.println("<head>");
                out.println("<tr>");
                    out.println("<th> id </th>");
                    out.println("<th> Nombre </th>");
                    out.println("<th> Cantidad </th>");
                    out.println("<th> Categoría </th>");
                    out.println("<th> Precio </th>");
                out.println("</tr>");
            out.println("</head>");


            out.println("<body>");
            for (Producto p : listaProd) {

                out.println("<tr>");
                    out.println("<td> " + p.getId() + " </td>");
                    out.println("<td> " + p.getNombre() + " </td>");
                    out.println("<td> " + p.getCantidad() + " </td>");
                    out.println("<td> " + p.getCategoria() + " </td>");
                    out.println("<td> " + p.getPrecio() + " </td>");
                out.println("</tr>");

            }
//                out.println("<tr>");
//                    out.println("<td> " + "Hola Trip..." + " </td>");
//                    out.println("<td> " + "Hola Trip..." + " </td>");
//                    out.println("<td> " + "Hola Trip..." + " </td>");
//                    out.println("<td> " + "Hola Trip..." + " </td>");
//                    out.println("<td> " + "Hola Trip..." + " </td>");
//                out.println("</tr>");
            out.println("</body>");
        out.println("</table>");


        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
