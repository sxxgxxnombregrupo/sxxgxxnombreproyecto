package persistencia_controlador;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.cj.jdbc.Driver;

public class ConexionDB  {

    // Atributos
    private String url = ""; // ruta de la base de datos
    private String host = "";
    private int port = 0;
    private String db = "";
    private String user = ""; // usuario para la db
    private String psw = ""; // constraseña parala db
    private Connection con = null; // conexión
    private Statement stmt = null; // estado de cuenta
    private ResultSet rs = null; // asignación de resultados

    // Controlador
    public ConexionDB() {
        // Class.forName("com.mysql.jdbc.Driver");  // Genera error en forName...
        // url = "jdbc:sqlite:j9.db";
        //url = "jdbc:sqlite:reto5db.db";  // DB anterior
        // ------------------------------------------------------------------------------
        // Cambió línea de código...
        //host = "db4free.net";
        host = "localhost";
        port = 3306;
        db = "db_tiendaonline";
        url = "jdbc:mysql://" + host + ":" + port + "/" + db;
//        url = "jdbc:mysql://localhost:3306/db_tiendaonline?serverTimezone=UTC";
        // url = "jdbc:mysql://db4free.net:3306/db_tiendaonline";
        // url = "jdbc:mysql://db4free.net:3306/prueba_productos";
        user = "misiontic_uis";
        psw = "misiontic.uis";
        //user = "root";
        //psw = "root123456";
        // ------------------------------------------------------------------------------
        try {
            //con = DriverManager.getConnection(url);
            // ------------------------------------------------------------------------------
            // Cambió línea de código...
            con = DriverManager.getConnection(url, user, psw);
            //con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_tiendaonline",
            //        "misiontic_uis", "misiontic.uis");
            // ------------------------------------------------------------------------------
            if (con != null){
                //con = null;
                DatabaseMetaData meta = con.getMetaData();
                System.out.println("Conexión establecida: ");
                System.out.println(meta.getDriverName());
            }
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
            System.out.println("Conexión Errónea");
        }
    }

    // Retorna la conexión
    public Connection getConnection(){
        return con;
    }

    // Cierra la conexión
    public void  closeConnection(Connection con){
        if (con != null){
            try {
                con.close();
            } catch (SQLException e){
                Logger.getLogger(ConexionDB.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    // Realizar consultas a la db => SELECT
    public ResultSet cunsultarBD(String sentencia){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sentencia);
        } catch (SQLException sqlex){
            System.out.println(sqlex.getMessage());
        } catch (RuntimeException rex){
            System.out.println(rex.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return rs;
    }

    // Insertar datos a db  => INSERT
    public boolean insertarBD(String sentencia){
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        }catch (SQLException | RuntimeException e){
            System.out.println("ERROR RUTINA: "+ e);
            return false;
        }
        return true;
    }

    // Borrar datos de la db  => DELETE
    public boolean barrarBD(String sentencia){
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        }catch (SQLException | RuntimeException e){
            System.out.println("ERROR RUTINA: " + e);
            return false;
        }
        return true;
    }

    // Actualizar datos de bd => UPDATE
    public boolean actualizarBD(String sentencia){
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(sentencia);
        }catch (SQLException | RuntimeException e){
            System.out.println("ERROR RUTINA: " + e);
            return false;
        }
        return true;
    }

    // Auto guardado
    public boolean setAutoCommitBD(boolean bol){
        try {
            con.setAutoCommit(bol);
        } catch (SQLException e){
            System.out.println("Error de configuración autoCommit " + e.getMessage());
            return false;
        }
        return true;
    }

    // cerrar conexión ya hay una función closeConnection()
    public void cerrarConexion(){
        closeConnection(con);
    }

    // confirmar guardar sentencia
    public boolean commitBD(){
        try {
            con.commit();
            return true;
        } catch (SQLException e){
            System.out.println("Error en commit " + e.getMessage());
            return false;
        }
    }

    // cancelar sentencia
    public boolean rollbackBD(){
        try {
            con.rollback();
            return true;
        } catch (SQLException e){
            System.out.println("Error en rollback " + e.getMessage());
            return false;
        }
    }

}
