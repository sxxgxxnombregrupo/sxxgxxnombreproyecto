# Importar el archivo de conexión de la base de datos
from conexiondb import conexion_db


# CRUD => Create - Read - Update - Delete

# Create =>
def insertar_datos(nombre, cantidad, categoria, precio):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # sql = "INSERT INTO productos4(id, nombre, cantidad, categoria, precio) VALUES(NULL, 'Costillitas', 60, 'Carnes', 16000)"
        sql = f"INSERT INTO productos(id, nombre, cantidad, categoria, precio) VALUES(NULL, '{nombre}', {cantidad}, '{categoria}', {precio})"
        cursor.execute(sql)
    conexion.commit()
    conexion.close()
    return "Datos guardados"

# Read => 
# leer todos los datos completos
def leer_datos():
    conexion = conexion_db()
    rstDB = []
    with conexion.cursor() as cursor:
        sql = "SELECT * FROM productos"
        cursor.execute(sql)
        rstDB = cursor.fetchall()
    conexion.close()
    return rstDB

# leer dato desde id
def leer_dato_id(id):
    conexion = conexion_db()
    rstDB = []
    with conexion.cursor() as cursor:
        sql = f"SELECT * FROM productos WHERE id = {id}"
        cursor.execute(sql)
        rstDB = cursor.fetchone()
    conexion.close()
    return rstDB

# Update =>
def actualizar_datos(id, nombre, cantidad, categoria, precio):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # id = 15
        sql = f"UPDATE productos SET nombre = '{nombre}', cantidad = {cantidad}, categoria = '{categoria}', precio = {precio} WHERE id = {id}"
        cursor.execute(sql)
        conexion.commit()
    cursor.close()
    return "Datos actualizados..."


# Delete =>
def eliminar_datos(id):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # id = 15
        sql = f"DELETE FROM productos WHERE id = {id}"
        cursor.execute(sql)
        conexion.commit()
    cursor.close()
    return "Datos eliminados..."