from flask import Flask, render_template, url_for, request, redirect
# Imprtar conexión con la base de datos
# from conexiondb import conexion_db
import controladordb


app = Flask(__name__)




@app.route('/')
def index(rstDB = None):
    
    if rstDB != None:
        rstDB = 'Variable Vacía 1'
    else:
        # # Línea 13 de "db.py"
        # # WebPage: https://parzibyte.me/blog/2021/03/29/flask-mysql-ejemplo-conexion-crud/
        # # conexion = pymysql.connect(host = "db4free.net", 
        # # conexion = pymysql.connect(host = "localhost", 
        # #                     port = 3306,
        # #                     user = "misiontic_uis",
        # #                     passwd = "misiontic.uis",
        # #                     db = "db_tiendaonline")
        
        # conexion = conexion_db()

        # # Línea 13 de "controlador_juego.py"
        # # WebPage: https://parzibyte.me/blog/2021/03/29/flask-mysql-ejemplo-conexion-crud/
        # rstDB = []
        # with conexion.cursor() as cursor:
        #     # sql = "SELECT id, nombre, cantidad, categoria, precio FROM productos WHERE id = %s", (id,)
        #     # cursor.execute(sql)
        #     # cursor.execute("SELECT id, nombre, cantidad, categoria, precio FROM productos")
        #     cursor.execute("SELECT * FROM productos")

        #     rstDB = cursor.fetchall()
        #     # rstDB = cursor.fetchone()
        # conexion.close()
        # # rstDB = 'rstDB Vacía 2'
        rstDB = controladordb.leer_datos()

    return render_template('index.html', 
                            rstDB = rstDB)


@app.route('/registrar_productos')
def registrar_productos():
    return render_template('registrar_productos.html')

@app.route('/guardar_datos', methods = ['GET', 'POST'])
def guardar_datos():
    # Recibir los datos del formulario
    # Debo importar el módulo request
    if request.method == "POST":
        nombre = request.form['nombre']
        cantidad = request.form['cantidad']
        categoria = request.form['categoria']
        precio = request.form['precio']

        rstDB = controladordb.insertar_datos(nombre, cantidad, categoria, precio)

        # return f"<h1>{rstDB}</h1>"
        return render_template('guardar_datos.html',
                                rstDB = rstDB)

    return redirect(url_for('index'))


@app.route('/editar_productos', methods = ['GET', 'POST'])
def editar_productos():
    if request.method == "POST":
        id = request.form['id']
        rstDB = controladordb.leer_dato_id(id)
        return render_template('editar_productos.html', 
                            rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/actualizar_datos', methods = ['GET', 'POST'])
def actualizar_datos():
    if request.method == "POST":
        id = request.form['id']
        nombre = request.form['nombre']
        cantidad = request.form['cantidad']
        categoria = request.form['categoria']
        precio = request.form['precio']
        rstDB = controladordb.actualizar_datos(id,
                nombre, cantidad, categoria, precio)
        # return f"<h1>{rstDB}</h1>"
        return render_template('actualizar_datos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/confirmar_eliminar_dato', methods = ['GET', 'POST'])
def confirmar_eliminar_dato():
    if request.method == 'POST':
        id = request.form['id']
        rstDB = controladordb.leer_dato_id(id)
        return render_template('confirmar_eliminar_dato.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/eliminar_datos', methods = ['GET', 'POST'])
def eliminar_datos():
    if request.method == 'POST':
        id = request.form['id']
        rstDB = controladordb.eliminar_datos(id)
        # return f"<h1>{rstDB}</h1>"
        return render_template('eliminar_datos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))




@app.route('/informacion')
def informacion():
    return render_template('informacion.html')

@app.route('/contenido')
def contenido():
    return render_template('contenido.html')


if __name__ == '__main__':
    # app.run(debug=True)
    app.run(debug=False)  # Utilizada para el despliegue...