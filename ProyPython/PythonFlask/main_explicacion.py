
# Importar módulo de flask
from flask import Flask, redirect   # redirec importada en la línea 67
from flask.helpers import url_for   # url_for importada en la línea 67

# Para utilizar plantillas, importamos el siguiente módulo
from flask import render_template
# creamos una nueva carpeta con el archivo index.html con su respectiva estructura
# mover el contenido (del return) a index


# Crear la aplicación, utilizando una variable
# Utilizamos el objeto Flask y pasarle la constante __name__
app = Flask(__name__)  # instanciamos el framework para poder utilizarlo

# para crear una ruta utilizamos @app.route y la ruta '/'
# q es la ruta inicial cuando abro el proyecto.
@app.route('/')
# para q la ruta esté asociada a un método, creo una función
def index(): # por el momento no recibe parámetro, únicamente retorna
    return "<h1> Aprendiendo flask... </h1>"
    # return render_template('index.html')   # utilizar después de la línea 76
    # Crear la carpeta templates y el archivo index.html
    


#############   Arrancar El Servidor en la Consola   ###########
# Me dirijo a la carpeta donde lo tengo creado el archivo main.py
# Ejecuto el archivo de python   =>   python main.py
# Reviso el url que debo ejecutar y lo hago en el navegador

# Es un framework muy sencillo donde puedo crear múltiples rutas

# Otras rutas creadas...

@app.route('/informacion')  # el parámetro es la url q tendremos
def informacion():   # el nombre de la función es la ruta
    return "Información..."

@app.route('/contacto')
def contacto():
    return "<h1>Contacto...</h1>"


@app.route('/rutaParametro')
@app.route('/rutaParametro/<nombre>')  # opción 1...
@app.route('/rutaParametro/<string:nombre>/<apellido>') # opción 2...
# def rutaParametro(nombre): # opción 1
def rutaParametro(nombre = None, apellido = None): # opción 2...

    texto = ""
    if nombre != None and apellido != None:
        texto = f"<h3> Bienvenido(a), {nombre} {apellido}"

    return f"""
    Diferente texto por renglon

    <h1> Renglón 3... </h1> 
    <h2> Renglón 3... </h2> 
    <h3> Renglón 3... </h3> 
    <h4> Renglón 4... </h4> 
    <h5> Renglón 5... </h5> 
    <h6> Renglón 6... </h6> 
    <br>
    {texto}
    """

    # <h1> Nombre... {nombre} </h1> 
    # <h1> Apellido... {apellido} </h1> 
    # """

@app.route('/redireccion')
@app.route('/redireccion/<redRuta>')
def redireccion(redRuta = None):
    if redRuta is not None:   # otra forma de especificar =>  if redRuta != None
        return redirect(url_for('contacto'))    # debo importar los módulos...
    return "<h1> No hay redireccionamiento..."


##############   Lo escribo primero q @app.route()   ##############
# creo el siguiente condicioal...
if __name__ == '__main__':   #  me permite identifica q es el fichero principal
    # el parametro debug en true es para q cuando 
    # arranque el servidor de flask funcione perfectamente
    # y cuando haga un cambio en el código, se actualice automáticamente
    app.run(debug=True)






